[fCC Forum Page](https://www.freecodecamp.org/forum/t/a-tribute-to-raccoons-feedback-requested/152652)

[Tribute Page](http://camper-fcc.gitlab.io/tribute/)
[Code](https://gitlab.com/camper-fcc/tribute)

Some features of the project:
- All native CSS, using flexbox and media queries -- no Bootstrap
- Responsive & mobile first design
- Fixed header
- [No inline styles](https://www.thoughtco.com/avoid-inline-styles-for-css-3466846)
- [No CSS ID selectors for styling](http://screwlewse.com/2010/07/dont-use-id-selectors-in-css/)
- Uses [semantic HTML](https://internetingishard.com/html-and-css/semantic-html/)
- Color scheme based on [Paletton color scheme](http://paletton.com/#uid=23P0u0k005l0jj201c-4j02cA0q).
- Passes [color contrast test](https://webaim.org/resources/contrastchecker/?fcolor=E2DFDF&bcolor=1D1D1D)
- Hosted on [GitLab Pages](https://gitlab.com/camper-fcc/tribute)
- Includes Favicon
- Uses [Open Sans](https://fonts.google.com/specimen/Open+Sans) and [Pragati Narrow](https://fonts.google.com/specimen/Pragati+Narrow) Google Fonts
- Uses [Normalize.css](https://necolas.github.io/normalize.css/)
- Links to [Font Awesome CDN](https://www.bootstrapcdn.com/fontawesome/)

All suggestions, critiques, or comments welcome. Thanks!